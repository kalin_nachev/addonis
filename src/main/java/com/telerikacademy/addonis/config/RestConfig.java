package com.telerikacademy.addonis.config;

import org.kohsuke.github.GitHub;
import org.kohsuke.github.GitHubBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Configuration
public class RestConfig {

    @Value("${git.api.username}")
    private String username;

    @Value("${git.api.password}")
    private String password;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.basicAuthentication(username, password).build();
    }

    @Bean
    public GitHub gitHub() {
        GitHub github = null;
        try {
            github = new GitHubBuilder().withOAuthToken(password, username).build();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return github;
    }
}
